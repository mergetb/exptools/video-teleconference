Note: Models are not stored in the repository. You must download GPT-2 models using `download_model.py`. Model choices are: 124M, 355M, 774M, 1558M.
